import requests
import yaml
from dns.zone import from_text
import logging

logging.basicConfig(filename='dns_script.log', format='[%(levelname)s] %(asctime)s %(message)s', level=logging.INFO)

def is_zone_file_valid(zone_file_path):
    try:
        with open(zone_file_path, 'r') as file:
            zone_content = file.read()
            logging.info(f'Reading the DNS zone file: {zone_file_path}')

            # Parse the zone file using dnspython
            zone = from_text(zone_content, relativize=False)

            # Validate the SOA record
            if not zone.get_rdataset('@', 'SOA'):
                raise ValueError("SOA record not found")

            # print(f'The zone file {zone_file_path} is valid.')
            logging.info(f'The zone file {zone_file_path} is valid.')
            return True

    except Exception as e:
        # print(f'The zone file {zone_file_path} is not valid. Error: {str(e)}')
        logging.error(f'The zone file {zone_file_path} is not valid. Error: {str(e)}')
        return False

def validate_yaml(file_path):
    try:
        with open(file_path, 'r') as file:
            # Use yaml.safe_load to parse the YAML content
            yaml_content = yaml.safe_load(file)
            # print("YAML file is valid.")
            logging.info(f'YAML file {file_path} is valid.')
            return yaml_content
    except yaml.YAMLError as e:
        # print(f"Error in YAML file: {e}")
        logging.error(f'Error in YAML file: {e}')
        return None

def post_dns_zone_file(zone_file, api_endpoint):
    try:
        logging.info(f'Going to upload DNS zone file')
        response = requests.post(api_endpoint, data=zone_file)
        response.raise_for_status()
        logging.info(f"DNS Zone update successful. Response: {response.text}")
        return response.raise_for_status()
    except requests.exceptions.RequestException as e:
        logging.error(f"Error updating DNS Zone. {e}")
        return None

if __name__ == '__main__':
    # YAML file path
    yaml_file_path = 'dns_data.yaml'

    # Origin section in DNS zone file
    refresh_times = """(
                    2023010101 ; serial
                    3600 ; refresh (1 hour)
                    1800 ; retry (30 minutes)
                    604800 ; expire (1 week)
                    86400 ; minimum (1 day)ust
                    )
    """

    # Validation of YAML file
    validated_data = validate_yaml(yaml_file_path)

    # Check if YAML data valid
    if validated_data:
        domain_name = validated_data[0]['name']
        yaml_data = validated_data[0]
        zone_file = ""

        # NS type record
        for ns in yaml_data.get('ns'):
            zone_file += f"@ 86400 IN NS {ns}\n"

        # MX type record
        for mx in yaml_data.get('mx'):
            zone_file += f"@ {'120'} IN MX {mx['priority']} {mx['value']}\n"

        # Rest of records
        for record in yaml_data['records']:
            if record['type'] == 'A':
                zone_file += f"{record['name']} {'120'} IN {record['type']} {record['value']}\n"
            elif record['type'] == 'CNAME':
                zone_file += f"{record['name']} {'120'} IN {record['type']} {record['value']}\n"
            elif yaml_data['mx'] == 'mx':
                zone_file += f"{record['name']} {'120'} IN {record['type']} {record['priority']} {record['value']}\n"
            elif record['type'] == 'TXT':
                zone_file += f"{record['name']} {'120'} IN {record['type']} '{record['value']}'\n"

        # Write DNS zone to zone.conf file
        try:
            with open('zone.conf', 'w') as file:
                 logging.info(f'Generating the zone.conf')
                 file.write("$ORIGIN " + domain_name + ". \n")
                 file.write('@ IN SOA ns1.example.com. admin.example.com. ')
                 file.write(refresh_times + '\n')
                 file.write(zone_file)  
            # Check if generated DNS zone valid
            zone_file_path = 'zone.conf' 
            is_valid = is_zone_file_valid(zone_file_path)
        except Exception as e:
            logging.error(f"An error occurred writing zone file: {e}")
    else:
        logging.error(f'Yaml data is not valid.')

    try:
        with open('zone.conf', 'r') as file:
            post_dns_zone_file(file, 'https://upload.api-enpoint.com/')
    except Exception as e:
        logging.error(f"An error occurred opening zone file while reading: {e}")
