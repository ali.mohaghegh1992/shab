### Scenario 1: DNS Configuration Automation
Overview
This Python script automates the process of updating DNS zone configurations for a dynamic environment using a DNS server with an API. The script reads a YAML file containing DNS record information, converts it into a zone file format, and then applies changes by sending a POST request to the DNS server API Endpoint.

This script check the validity of a YAML and DNS files.

To run this script, first create an python venv and activate it:
```bash
python3 -m venv <virtual-environment-name>
source env/bin/activate
```
Tehn install python requier packages:
```bash
pip install -r requirements.txt
```
This scripty need an YAML file which contain the DNS zone congis, name it ``dns_data.yaml`` and run the script:
```bash
python3 main.py
```
If everything goes right your generate DNS zone file write to a file named ```zone.conf``` and it goes to the API endpoing, and if something happend all logged into a file named ```dns_script.log``` that you can check.
