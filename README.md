Two senarios are here, one about DNS and the other is measuring Mysql query execution time which each of them comes as follow here in the two directory.
```
.
├── README.md
├── dns
│   ├── README.md
│   ├── dns_data.yaml
│   ├── dns_script.log
│   ├── main.py
│   └── zone.conf
└── mysql
    ├── Dockerfile
    ├── app
    │   ├── main.py
    │   └── requirements.txt
    ├── docker-compose.yml
    ├── grafana
    │   ├── config
    │   │   └── config.yaml
    │   ├── dashboard
    │   │   └── mysql-dashboard.json
    │   └── datasource
    │       └── datasource.yaml
    ├── init.sql
    └── prometheus
        └── prometheus.yml
```
