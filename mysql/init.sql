CREATE DATABASE IF NOT EXISTS mydb;

USE mydb;

CREATE TABLE IF NOT EXISTS students (
    id INT AUTO_INCREMENT PRIMARY KEY,
    name VARCHAR(255),
    age INT,
    major VARCHAR(255)
);

INSERT INTO students (name, age, major) VALUES
    ('Alice', 20, 'Computer Science'),
    ('Bob', 22, 'Mathematics'),
    ('Charlie', 21, 'Physics');
