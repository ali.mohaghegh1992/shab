### Scenario 2: Monitoring MySQL Query Performance with Prometheus
Overview
This application connects to a MySQL database, executes queries, and monitors the time it takes for the entire process, including connection time, query execution time, and response time. The monitoring data is exposed using the Prometheus SDK and visualized through Prometheus in a Dockerized environment.

# Docker and docker compose needed to be installed.

To run this scenario,first clone this repo via:
```bash
https://gitlab.com/ali.mohaghegh1992/shab.git
cd mysql
```
Second run:
```bash
docker compose up --build -d
```

Few second wait for getting a running status from each container then you can see the dashboard in Grafana, named ```Mysql Query Dashboard```
