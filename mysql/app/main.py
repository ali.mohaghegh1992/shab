import time
from flask import Flask
from prometheus_client import start_http_server, Summary, Counter, generate_latest
import pymysql

app = Flask(__name__)
# Prometheus query objects
connection_time = Summary('mysql_connection_time_seconds', 'Time taken to establish MySQL connection')
query_time = Summary('mysql_query_time_seconds', 'Time taken to execute MySQL query')
response_time = Summary('mysql_response_time_seconds', 'Total time for the entire process')
query_counter = Counter('mysql_query_count', 'Number of MySQL queries executed')

@app.route('/')
@response_time.time()
def execute_query():
    start_time = time.time()
    conn = pymysql.connect(host='mysql', user='root', password='password', database='mydb')
    end_time = time.time()
    connection_time.observe(end_time - start_time)
    # Start to excute mysql query
    cursor = conn.cursor()
    query_start_time = time.time()
    cursor.execute('SELECT 1 ;')
    query_end_time = time.time()
    query_time.observe(query_end_time - query_start_time)
    result = cursor.fetchall()
    cursor.close()
    conn.close()
    query_counter.inc()
    # return str(result) - All records throw to the user face
    return "OK"

@app.route('/metrics')
def metrics():
    # Expose custom metrics
    return generate_latest()

if __name__ == '__main__':
    # start_http_server(8031)
    app.run(debug=True, host='0.0.0.0', port=5031)
